from flask_oidc import OpenIDConnect
from app import app

class CONFIGURATION:
    def keycloakConfiguration():
        app.config.from_mapping(
                SECRET_KEY='ec75a5e7-76cd-4742-99c4-85b6f8dfe3a6',
                OIDC_CLIENT_SECRETS='./keycloak.json',
                OIDC_VALID_ISSUERS=['http://localhost:2000/auth/realms/dk-realm'],
                # OIDC_INTROSPECTION_AUTH_METHOD='client_secret_post',
                OIDC_TOKEN_TYPE_HINT='access_token',
            )

        oidc = OpenIDConnect(app)
        return oidc